# Descomplicando-o-gitlab

Treinamento Descomplicando o Gitlab criado ao vivo na Twitch

###Day-1

```bash
- Entendemos o que é Git
- Entendemos o que é Woeking Dir, Index e HEAD
- Entendemos o que é Gitlab
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
- Como criar uma branch
- Como criar um merge request
- Como adicionar um membro no projeto
- Como fazer o merge na Master/mai
- Como associar um repo local com um repo remoto
- Como importar um reto do Github para o Gitlab
- Mudamos a branch padrão para Main
```
